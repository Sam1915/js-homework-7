let items = ['hello', 38, 'world', 23, '23', null,];

const filterBy = (array, dataType) => array.filter(item => typeof item !== dataType);

console.log(filterBy(items, "number"));